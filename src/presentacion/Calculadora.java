package presentacion;

import java.util.Scanner;

public class Calculadora {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        float num1 = 0, num2 = 0;
        int opcion;

        do {
            System.out.println("Elegir una operacion aritmetica a realizar :");
            System.out.println("1. Sumar");
            System.out.println("2. Restar");
            System.out.println("3. Multiplicar");
            System.out.println("4. Dividir");
            System.out.println("5. Factorial");
            System.out.println("6. Potencia");
            System.out.println("7. Salir");
            System.out.print("Opcion: ");
            opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    System.out.print("Ingrese la primera variable a calcular: ");
                    num1 = scanner.nextFloat();
                    System.out.print("Ingrese la segunda variable a calcular: ");
                    num2 = scanner.nextFloat();
                    System.out.println("La suma es: " + sumar(num1, num2));
                    break;
                case 2:
                    System.out.print("Ingrese la primera variable a calcular: ");
                    num1 = scanner.nextFloat();
                    System.out.print("Ingrese la segunda variable a calcular: ");
                    num2 = scanner.nextFloat();
                    System.out.println("La resta es: " + restar(num1, num2));
                    break;
                case 3:
                    System.out.print("Ingrese la primera variable a calcular: ");
                    num1 = scanner.nextFloat();
                    System.out.print("Ingrese la segunda variable a calcular: ");
                    num2 = scanner.nextFloat();
                    System.out.println("La multiplicacion es: " + multiplicar(num1, num2));
                    break;
                case 4:
                    System.out.print("Ingrese la primera variable a calcular: ");
                    num1 = scanner.nextFloat();
                    System.out.print("Ingrese la segunda variable a calcular: ");
                    num2 = scanner.nextFloat();
                    try {
                        System.out.println("La división es: " + dividir(num1, num2));
                    } catch (ArithmeticException e) {
                        System.out.println("Error: Al Dividir por cero");
                    }
                    break;
                case 5:
                    int num;
                    System.out.print("Ingrese un número para calcular su factorial: ");
                    num = scanner.nextInt();
                    System.out.println("El factorial es: " + factorial(num));
                    break;
                case 6:
                    System.out.print("Ingrese la base: ");
                    num1 = scanner.nextFloat();
                    System.out.print("Ingrese el exponente: ");
                    int num21 = scanner.nextInt();
                    System.out.println("La potencia es: " + potencia(num1, num21));
                    break;
                case 7:
                    System.out.println("Finalizando el programa...");
                    break;
                default:
                    System.out.println("Opcion no valida. Por favor, elija una opcion valida.");
                    break;
            }
        } while (opcion != 7);

        scanner.close();
    }

    public static double factorial(int num) {
        double factorial = 1;
        for (int i = 1; i <= num; i++) {
            factorial *= i;
        }
        return (double) factorial;
    }

    public static double potencia(float base, int exponente) {
        return Math.pow(base, exponente);
    }

    public static double dividir(float num1, float num2) {
        return num1 / num2;
    }

    public static float sumar(float num1, float num2) {
        return num1 + num2;
    }

    public static float restar(float num1, float num2) {
        return num1 - num2;
    }

    public static float multiplicar(float num1, float num2) {
        return num1 * num2;
    }
}                